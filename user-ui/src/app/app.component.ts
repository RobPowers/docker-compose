import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from 'app/store';
import { Observable } from 'rxjs/Observable';
import { User } from 'app/store/models';

import 'rxjs/add/operator/let';
import * as _ from 'lodash';
import {getMeState} from 'app/store/selectors';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public me$: Observable<User>;

  constructor(private store: Store<AppState>) {
    this.me$ = this.store.select(getMeState);
  }

  isAdmin(me): boolean {
    return me.roles.indexOf('ROLE_ADMIN') !== -1;
  }
}
