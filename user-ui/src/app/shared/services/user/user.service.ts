import {Injectable} from "@angular/core";
import {Http, Response} from "@angular/http";

import {Observable} from "rxjs/Observable";
import "rxjs/add/operator/map";

import {Role, User} from "app/store/models";
import {isNullOrUndefined} from "util";

@Injectable()
export class UserService {

  constructor(private http: Http) { }

  getMe(): Observable<User> {
    return this.http
      .get('/api/whoami')
      .map(response => response.json())
      .catch(err => '');
  }

  getMyRoles(): Observable<Role[]> {
    return this.http
      .get(`/api/whoami/roles`)
      .map(response => response.json())
      .catch(err => '');
  }

  getUsers(): Observable<User[]> {
    return this.http
      .get('/api/users')
      .map((response: Response) => response.json())
      .catch(err => '');
  }

  getUser(uid: string = ''): Observable<User> {
    if (isNullOrUndefined(uid) || uid.trim() === '') {
      return this.getMe();
    } else {
      return this.http
        .get('/api/users/' + uid)
        .map(response => response.json())
        .catch(err => '');
    }
  }
}
