import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MdIconModule, MdButtonModule } from '@angular/material';

import { ProfileRoutingModule } from './profile-routing.module';
import { ProfilePageComponent } from './profile-page/profile-page.component';

@NgModule({
  imports: [
    CommonModule,
    ProfileRoutingModule,
    MdIconModule,
    MdButtonModule
  ],
  declarations: [ProfilePageComponent]
})
export class ProfileModule { }
