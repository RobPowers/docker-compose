import {Component} from '@angular/core';
import {Store} from '@ngrx/store';
import {AppState} from 'app/store';
import {Observable} from 'rxjs/Rx';

import {User} from 'app/store/models';
import {getProfileState} from 'app/store/selectors';
import {LoadProfileAction} from 'app/store/actions';

import 'rxjs/add/operator/let';
import {ActivatedRoute, Params} from '@angular/router';

@Component({
  selector: 'app-profile-page',
  templateUrl: './profile-page.component.html',
  styleUrls: ['./profile-page.component.css']
})
export class ProfilePageComponent {
  public profile$: Observable<User>;

  constructor(private store: Store<AppState>, private route: ActivatedRoute) {
    this.profile$ = this.store.select(getProfileState);
    this.route.params.subscribe((params: Params) => {
      this.store.dispatch(new LoadProfileAction(params['uid']));
    });
  }
}
