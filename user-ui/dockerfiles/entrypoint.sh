#!/bin/bash

APP_DIR=/user-ui

function update_dependencies {
  cd ${APP_DIR}
  yarn install
}

case "$1" in
  run)
    if [ ! -d ${APP_DIR}/node_modules ]; then
      update_dependencies
    fi
    cd ${APP_DIR}
    yarn start
    ;;
  install)
    update_dependencies
    ;;
  *)
    $*
    ;;
esac
