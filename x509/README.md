# X.509 Certificates

The `x509` directory contains the server and user certificates supported by this set of services.

The certificates are divided into three directories:

-   `ca`: The certificate authority key and certificate
-   `client`: The client certificates (PKCS#12 bundles)
-   `server`: PEM files used by Nginx

## Generating the Server Certificate
 
The server certificate defines several Subject Alternate Names (SANs) in addition to its primary
name, `localhost`:

-   `proxy`
-   `nginx`
-   `proxy.demo.bti360.com`
-   `nginx.demo.bti360.com`
-   `localhost.localdomain`
-   `127.0.0.1`
-   `192.168.99.100`

We need a custom configuration file (`server/proxy.openssl.cnf`) to generate the signing request
and signed certificate for the server. The following commands are used to create/update this
certificate and key.

```bash
cd ${PROJECT_ROOT}/x509/server
# Create the private key and CSR
openssl req -config proxy.openssl.cnf \
            -out server.csr \
            -keyout server.key \
            -new \
            -newkey rsa:2048 \
            -nodes

# Sign the certificate with the demo CA
openssl x509 -req \
             -extfile proxy.openssl.cnf \
             -extensions x509_ext \
             -days 365 \
             -in server.csr \
             -CA ../ca/ca.crt \
             -CAkey ../ca/ca.key \
             -set_serial 01 \
             -out server.crt
             
# Remove the CSR
rm server.csr
```

## Generating Client Certificates

The client certifcates can be generated with `openssl` and signed with the CA certificate (`ca/ca.crt`, `ca/ca.key`).
When signing certificates, a unique serial number must be used for each certificate. See below for
the list of assigned serial numbers.

Client certificates can be generated with the script `client/new-client.sh`.

Usage:

```bash
new-client.sh <userid> <serial_number>
```

Example:

```bash
# Regenerate the btiadmin.p12 certificate
new-client.sh btiadmin 2
```

## Serial Numbers

| Serial | Certificate  |
|:------:|--------------|
| 1      | server.crt   |
| 2      | btiadmin.crt |
| 3      | btiuser.crt  |
