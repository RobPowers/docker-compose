package com.bti360.users;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface UserService {
    /**
     * Get a user by username.
     *
     * @param username the username
     * @return the requested user
     */
    Map<String, Object> getUserByUsername(String username);

    /**
     * Get the requested profile picture for the target user.
     *
     * @param username the username
     * @param imageSize the image size
     * @param imageFormat the output format
     * @return the bytes of the requested image
     * @throws IOException if an error occurs retrieving or formatting the image
     */
    byte[] getProfilePicture(String username, ImageSize imageSize, String imageFormat) throws IOException;

    /**
     * Search the user database.
     *
     * @param search the search query
     * @return the List of users matching the query
     */
    List<Map<String, Object>> searchUsers(String search);
}
