package com.bti360.users;

import java.util.Set;

public interface UserRoleService {
    /**
     * Get the roles for the specified user.
     *
     * @param username the user name
     * @return the roles assigned to the target user
     */
    Set<UserRole> getRolesForUser(String username);
}
