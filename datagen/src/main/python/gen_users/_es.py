import json
import logging

import elasticsearch.helpers
import os
from elasticsearch import Elasticsearch, NotFoundError

_SCHEMA_FILE = os.path.join(os.path.dirname(__file__), 'user-schema.json')

_log = logging.getLogger('datagen')


class EsHelper(object):
    def __init__(self, es_host, users):
        """
        :param es_host: the Elasticsearch host
        :type es_host: str
        :param users: the users
        :type users: list(RandomUser)
        """
        super(EsHelper, self).__init__()
        self._es_host = es_host
        self._users = users

    def populate_user_index(self):
        _log.info("Populating %s index on %s", self._index_name, self._es_host)
        self._create_index()
        self._load_users()

    def _create_index(self):
        try:
            _log.info("Deleting old index: %s", self._index_name)
            self.es.indices.delete(index=self._index_name)
        except NotFoundError:
            _log.debug("Index %s did not exist", self._index_name)
            pass

        with open(_SCHEMA_FILE) as f:
            user_schema = json.loads(f.read())
        _log.debug("User Schema: %s", json.dumps(user_schema, indent=2))

        _log.info("Creating index: %s", self._index_name)
        self.es.indices.create(index=self._index_name, body=user_schema)

    def _load_users(self):
        _log.info("Indexing Users into index: %s", self._index_name)
        elasticsearch.helpers.bulk(self.es, actions=self._user_actions)

    @property
    def _index_name(self):
        return 'random-users'

    @property
    def _user_actions(self):
        actions = [dict(_index=self._index_name,
                        _type='User',
                        _id=user.uid,
                        _source=user.es) for user in self._users]

        _log.debug("Actions:")
        for action in actions:
            _log.debug(str(action))

        return actions

    @property
    def es(self):
        """
        :return: an Elasticsearch connection
        :rtype: elasticsearch.ElasticSearch
        """
        return Elasticsearch(hosts=[self._es_host])
